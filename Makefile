include config.mk

NAME=dwmstatus

# the build target executable:
TARGET=dwmstatus

all: options $(TARGET)

options:
	@echo ${NAME} build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "CC       = ${CC}"

$(TARGET): src/$(TARGET).c
	$(CC) $(CFLAGS) -o $(TARGET) src/$(TARGET).c

clean:
	rm -f $(TARGET)

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f ${NAME} ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/${NAME}

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/${NAME}
	rm -rf ${DESTDIR}${PREFIX}/bin

PHONY: all options $(TARGET) clean install uninstall
