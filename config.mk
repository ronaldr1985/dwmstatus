CC = clang

# Path
PREFIX = /usr/local

X11INC = /usr/X11R6/include
X11LIB = /usr/X11R6/lib

INCS = -I${X11INC}
LIBS = -lX11

CFLAGS  = -g -Wall -std=c99 -pedantic $(INCS) $(LIBS)
