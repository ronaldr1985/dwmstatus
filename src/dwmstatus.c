#include "dwmstatus.h"

char *strInsert(char *str1, const char *str2, int pos)
{
	size_t l1 = strlen(str1);
	size_t l2 = strlen(str2);

	if (pos <  0) pos = 0;
	if (pos > l1) pos = l1;

	char *p  = str1 + pos;
	memmove(p + l2, p, l1 - pos);
	memcpy (p, str2,  l2);
	return str1;
}

int cpuUsagePerc(void)
{
	int perc;
	long double a[4], b[4];
	FILE *fp;

	fp = fopen("/proc/stat", "r");
	if (fp == NULL) {
		printf("Failed to open file /proc/stat");
		return 1;
	}
	fscanf(fp, "%*s %Lf %Lf %Lf %Lf", &a[0], &a[1], &a[2], &a[3]);
	fclose(fp);

	sleep(delay);

	fp = fopen("/proc/stat", "r");
	if (fp == NULL) {
		printf("Failed to open file /proc/stat");
		return 1;
	}
	fscanf(fp, "%*s %Lf %Lf %Lf %Lf", &b[0], &b[1], &b[2], &b[3]);
	fclose(fp);

	perc = 100 * ((b[0]+b[1]+b[2]) - (a[0]+a[1]+a[2])) / ((b[0]+b[1]+b[2]+b[3]) - (a[0]+a[1]+a[2]+a[3]));
	return (perc);
}

int ramUsagePerc(void)
{
	long total, free, buffers, cached;
	FILE *fp;

	fp = fopen("/proc/meminfo", "r");
	if (fp == NULL) {
		printf("Failed to open file /proc/meminfo");
		return 1;
	}
	fscanf(fp, "MemTotal: %ld kB\n", &total);
	fscanf(fp, "MemFree: %ld kB\n", &free);
	fscanf(fp, "MemAvailable: %ld kB\nBuffers: %ld kB\n", &buffers, &buffers);
	fscanf(fp, "Cached: %ld kB\n", &cached);
	fclose(fp);

	return (100 * ((total - free) - (buffers + cached)) / total);
}

void getDateTime(char *format, char *formattedDateTime)
{
	time_t rawtime;
	struct tm *timeptr;

	time(&rawtime);
	timeptr = localtime(&rawtime);
	strftime(formattedDateTime, 80, format, timeptr);
}

double mountpointAvailGB(const char *mountpoint)
{
	struct statfs mountpointStatfs;

	statfs(mountpoint, &mountpointStatfs);

	return ((long double)mountpointStatfs.f_bfree * mountpointStatfs.f_bsize)/1073741824;
}

double mountpointTotalGB(const char *mountpoint)
{
	struct statfs mountpointStatfs;

	statfs(mountpoint, &mountpointStatfs);

	return ((long double)mountpointStatfs.f_blocks * mountpointStatfs.f_bsize)/1073741824;
}

double mountpointUsedGB(const char *mountpoint)
{
	struct statfs mountpointStatfs;

	statfs(mountpoint, &mountpointStatfs);

	return (((long double)mountpointStatfs.f_blocks * mountpointStatfs.f_bsize) - ((long double)mountpointStatfs.f_bfree * mountpointStatfs.f_bsize))/1073741824;
}

double mountpointUsedPerc(const char *mountpoint)
{
	return (mountpointUsedGB(mountpoint)/mountpointTotalGB(mountpoint)) * 100;
}

int batteryPerc(char BATTERY[6])
{
	int perc;
	int position = 24;
	FILE *fp;
	char bat[48] = "/sys/class/power_supply//capacity";
	fp = fopen (strInsert(bat,BATTERY,position), "r");
	if (fp == NULL ) {
		perror("Error: ");
		return(-1);
	}
	fscanf(fp, "%i", &perc);
	fclose(fp);
	return (perc);
}

int main(int argc, char * argv[]) {
	Display * dpy = NULL;
	char output[BUFSIZE];
	char dateTime[64];

	dpy = XOpenDisplay(getenv("DISPLAY"));
	if (dpy == NULL) {
		fprintf(stderr, "Can't open display, exiting.\n");
		exit(1);
	}

	for (;;) {
		getDateTime("%A %d %B %Y %X", dateTime);
		sprintf(output,
				"home: %.1lfGB/%.1lfGB | root: %.1lfGB/%.1lfGB | CPU: %d%% | RAM: %d%% | %s ",
				mountpointUsedGB("/home"), mountpointTotalGB("/home"), mountpointUsedGB("/"), mountpointTotalGB("/"), cpuUsagePerc(), ramUsagePerc(), dateTime);
		XStoreName(dpy, DefaultRootWindow(dpy), output);
		XSync(dpy, False);
	}
	XFlush(dpy);
	return 0;
}

