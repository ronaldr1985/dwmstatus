#include <X11/Xlib.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/vfs.h>
#include <time.h>
#include <unistd.h>

#include "config.h"

#define BUFSIZE 8196

char *strInsert(char *str1, const char *str2, int pos);
int cpuUsagePerc(void);
int ramUsagePerc(void);
void getDateTime(char *format, char *formattedDateTime);
double mountpointAvailGB(const char *mountpoint);
double mountpointTotalGB(const char *mountpoint);
double mountpointUsedGB(const char *mountpoint);
double mountpointPerc(const char *mountpoint);
int batteryPerc(char BATTERY[6]);
int numbProcesses(void);

